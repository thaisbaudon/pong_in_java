import javafx.scene.Node;

public interface Displayable {
    Node getNode();
}
