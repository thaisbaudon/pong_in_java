import javafx.scene.shape.Rectangle;
import javafx.scene.Node;
import javafx.beans.property.*;
import javafx.scene.paint.Color;

public abstract class AbstractRacket implements Displayable, Bounceable {
    AbstractRacket(double _xPos, double _yPos) {
	xPos = new SimpleDoubleProperty(_xPos);
	yPos = new SimpleDoubleProperty(_yPos);
	rectangle = new Rectangle(_xPos, _yPos, width, height);
	rectangle.setFill(Color.BLACK);
	
 	// Synchronize the internal coords with the displayed coords
	rectangle.xProperty().bind(xPos);
	rectangle.yProperty().bind(yPos);
   }

    // Displayable
    @Override
    public Node getNode() {
	return rectangle;
    }

    // Bounceable
    @Override
    public Vector bounce(Ball ball) {
	return new Vector(- ball.getSpeed().x, - ball.getSpeed().y);
    }

    @Override
    public boolean ballHit(Ball ball) {
	double ballX = ball.getXPos();
	double ballY = ball.getYPos();
	double x = xPos.get();
	double y = yPos.get();
	double radius = ball.getRadius();

	boolean hits = (ballX - radius <= x + width);
	hits = hits && (ballX + radius >= x);
	hits = hits && (ballY - radius <= y + height);
	hits = hits && (ballY + radius >= y);
	return hits;
    }

    public boolean ballHitVertical(Ball ball) {
	double ballX = ball.getXPos();
	double x = xPos.get();
	double radius = ball.getRadius();

	boolean hits = (ballX - radius <= x + width);
	hits = hits && (ballX + radius >= x);
	return hits;
    }

    DoubleProperty xPosProperty() {
	return xPos;
    }

    double getXPos() {
	return xPos.get();
    }

    DoubleProperty yPosProperty() {
	return yPos;
    }

    double getYPos() {
	return yPos.get();
    }

    static double getStep() {
	return step;
    }

    static double getHeight() {
	return height;
    }

    static double getWidth() {
	return width;
    }

    boolean isMovingUp() {
	return movingUp;
    }

    boolean isMovingDown() {
	return movingDown;
    }

    Rectangle rectangle; 
    DoubleProperty xPos;
    DoubleProperty yPos;
    boolean movingUp;
    boolean movingDown;
    static double step = 1;
    static double width = 2;
    static double height = 50;
}
