import javafx.util.Duration;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.event.ActionEvent;
import javafx.beans.value.ChangeListener;
import javafx.stage.*;
import javafx.scene.*;
import javafx.application.Application;
import javafx.animation.*;

public class Engine extends InitEngine {
    Engine(Scene _scene, boolean leftAuto, boolean rightAuto) {
	scene = _scene;

	initRackets(leftAuto, rightAuto);
	score = new Score(field.getCenterPos());
	initNodes();
	initTimelines();
    }
    
    @Override
    protected void updateRackets(ActionEvent e) {
	racketUpdate(leftRacket);
	racketUpdate(rightRacket);
    }
    
    private void racketUpdate(AbstractRacket racket) {
	double minY = field.getTopWallY();
	double maxY = field.getBottomWallY();

	double nextYPos = racket.getYPos();
	double step = AbstractRacket.getStep();

	if (racket.isMovingUp()
		&& racket.getYPos() > minY)
	    nextYPos -= step;

	else if (racket.isMovingDown()
		    && racket.getYPos() + racket.getHeight() < maxY)
	    nextYPos += step;

	KeyFrame nextKeyFrame = new KeyFrame(
	    racketTimeline.getCurrentTime().add(
			Duration.millis(racketDt)),
	    new KeyValue(racket.yPosProperty(), nextYPos));

	racketTimeline.getKeyFrames().add(nextKeyFrame);
	racketTimeline.playFrom(racketTimeline.getCurrentTime());
    }

    @Override
    protected void updateBall(ActionEvent e) {
	ballUpdate();
    }

    void ballUpdate() {
	// Compute the next relevant position of the ball and the time it needs to reach it, then add it to the ball timeline
	
	KeyFrame nextKeyFrame;

	// Reset if the ball is out of bounds
	// WORK IN PROGRESS
	if (field.outOfBounds(ball)) {
	    //ballTimeline = new Timeline();
	
	    ball.pickRandomSpeed();
	    // Update scores
	    if (leftRacket.ballHitVertical(ball))
		score.incrementRight();

	    else if (rightRacket.ballHitVertical(ball))
		score.incrementLeft();
	    
	    //ball.reset(field.getCenterPos());
	    nextKeyFrame = new KeyFrame(ballTimeline.getCurrentTime().add(Duration.millis(1)),
		    new KeyValue(ball.xPosProperty(), field.getCenterPos().x), new KeyValue(ball.yPosProperty(), field.getCenterPos().y));

	    // Wait
	    //KeyFrame wait = new KeyFrame(Duration.millis(resetTime));
	    //ballTimeline.getKeyFrames().add(wait);
	}

	else
	    nextKeyFrame = nextBallKeyFrame();
	    
	ballTimeline.getKeyFrames().add(nextKeyFrame);
	ballTimeline.playFrom(ballTimeline.getCurrentTime());
    }

    private KeyFrame nextBallKeyFrame() {
        // Initialize in case the ball doesn't hit anything
	Vector newSpeed = ball.getSpeed();

	// Avoid getting stuck/failing to escape from a racket
	boolean noHitLeft = false;
	boolean noHitRight = false;

	// Bounce against the hit object, if any
	if (leftRacket.ballHit(ball)) {
	    noHitLeft = true;
	    newSpeed = leftRacket.bounce(ball);
	}

	else if (rightRacket.ballHit(ball)) {
	    noHitRight = true;
	    newSpeed = rightRacket.bounce(ball);
	}

	else if (field.ballHit(ball))
	    newSpeed = field.bounce(ball);

	ball.setSpeed(newSpeed);
	Vector nextPos = nextBallPosition(noHitLeft, noHitRight);
	double time = timeToNextPos(nextPos);

	KeyValue xKeyVal = new KeyValue(
	    ball.xPosProperty(), nextPos.x);

	KeyValue yKeyVal = new KeyValue(
	    ball.yPosProperty(), nextPos.y);

	return new KeyFrame(
	    ballTimeline.getCurrentTime().add(Duration.millis(time)),
	    xKeyVal, yKeyVal);
    }

    private Vector nextBallPosition(boolean noHitLeft, boolean noHitRight) {
	Vector currentPos = ball.getPos();
	Vector newPos = ball.getPos();
	Vector speed = ball.getSpeed();
	Ball newBall = new Ball(newPos.x, newPos.y);
	
	do {
	    newPos = newPos.add(speed);
	    newBall = new Ball(newPos.x, newPos.y);
	} while (!field.ballHit(newBall)
		&& !(leftRacket.ballHitVertical(newBall) && !noHitLeft)
		&& !(rightRacket.ballHitVertical(newBall) && !noHitRight));

	return newPos;
    }

    private double timeToNextPos(Vector nextPos) {
	double distance =
	    nextPos.distanceTo(ball.getXPos(), ball.getYPos());
	// t = d/v
	double scalarSpeed = ball.getSpeed().norm();
	return distance / scalarSpeed;
    }

    private static double racketDt = 1;
    private static double resetTime = 1000;
}
