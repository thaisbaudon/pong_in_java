
public interface Bounceable {
    Vector bounce(Ball ball);
    boolean ballHit(Ball ball);
}
