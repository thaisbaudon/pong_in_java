import java.lang.Math.*;

public class Vector {
    Vector() {
	x = 0.0;
	y = 0.0;
    }

    Vector(double _x, double _y) {
	x = _x;
	y = _y;
    }

    // Copy constructor
    Vector(Vector v) {
	x = v.x;
	y = v.y;
    }

    Vector minus() {
	return new Vector(-x, -y);
    }

    static Vector minus(Vector v) {
	return v.minus();
    }

    Vector add(Vector v) {
	return new Vector(x + v.x, y + v.y);
    }

    static Vector plus(Vector v1, Vector v2) {
	return new Vector(v1.x + v2.x, v1.y + v2.y);
    }

    static Vector minus(Vector v1, Vector v2) {
	return plus(v1, minus(v2));
    }

    double norm() {
	return Math.sqrt(x * x + y * y);
    }

    double distanceTo(Vector v) {
	return add(v.minus()).norm();
    }

    double distanceTo(double vx, double vy) {
	return distanceTo(new Vector(vx, vy));
    }

    Vector mult(double coef) {
	return new Vector(coef * x, coef * y);
    }

    double x;    
    double y;    
}
