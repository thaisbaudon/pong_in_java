import java.util.Map;
import javafx.util.Duration;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.event.ActionEvent;
import javafx.beans.value.ChangeListener;
import javafx.stage.*;
import javafx.scene.*;
import javafx.application.Application;
import javafx.animation.*;

public class Main extends Application {
    static void main(String[] args) {
	System.out.println(String.valueOf(args));
	leftAuto = (args[0] == "auto");
	rightAuto = (args[1] == "auto");
	launch(args);
    }

    /*boolean isLeftAuto(Application.Parameters paramList) {
	Map<String,String> params = paramList.getNamed();
	return (params.get("left") == "auto");
    }

    boolean isRightAuto(Application.Parameters paramList) {
	Map<String,String> params = paramList.getNamed();
	return (params.get("right") != "user");
    }*/

    @Override
    public void init() throws Exception {
	super.init();
//	params = getParameters();
    }

    @Override
    public void start(Stage stage) {
	Scene scene = new Scene(new Group(), 800, 600);

	Engine engine = new Engine(scene, leftAuto, rightAuto);
	
	stage.setScene(scene);
	stage.setTitle("Pong");
	stage.show();
    }

    static private boolean leftAuto = false;
    static private boolean rightAuto = true;
}
