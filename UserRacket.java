import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;

public class UserRacket extends AbstractRacket {
    UserRacket(double _xPos, double _yPos) {
	super(_xPos, _yPos);
    }

    UserRacket(double _xPos, double _yPos, KeyCode _up, KeyCode _down) {
	super(_xPos, _yPos);
	upKey = _up;
	downKey = _down;
    }

    void keyPressHandler(KeyEvent e) {
	// Start moving up or down
	movingUp = false;
	movingDown = false;

	if (e.getCode() == upKey)
	    movingUp = true;
	
	else if (e.getCode() == downKey)
	    movingDown = true;
    }

    void keyReleaseHandler(KeyEvent e) {
	// Stop moving
	if (e.getCode() == upKey)
	    movingUp = false;
	
	else if (e.getCode() == downKey)
	    movingDown = false;
    }

    private KeyCode upKey = KeyCode.UP;
    private KeyCode downKey = KeyCode.DOWN;
}
