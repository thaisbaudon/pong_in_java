import javafx.util.Duration;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.event.ActionEvent;
import javafx.beans.value.ChangeListener;
import javafx.stage.*;
import javafx.scene.*;
import javafx.application.Application;
import javafx.animation.*;

abstract class InitEngine {
    void initNodes() {
	Group root = new Group();

	root.getChildren().add(ball.getNode());
	root.getChildren().add(field.getNode());
	root.getChildren().add(leftRacket.getNode());
	root.getChildren().add(rightRacket.getNode());
	root.getChildren().add(score.getNode());

	scene.setRoot(root);
    }

    protected UserRacket initUserRacket(double x, double y, KeyCode upKey, KeyCode downKey) {
	UserRacket racket = new UserRacket(x, y, upKey, downKey);

	// Handle user input
	scene.setOnKeyPressed((KeyEvent e) -> racket.keyPressHandler(e));
	scene.setOnKeyReleased((KeyEvent e) -> racket.keyReleaseHandler(e));

	return racket;
    }

    protected AIRacket initAutoRacket(double x, double y) {
	AIRacket racket = new AIRacket(x, y);

	// Handle ball events
	ChangeListener<Number> listener =
	    (obs, oldVal, newVal) -> racket.computeMovement(ball);

	ball.xPosProperty().addListener(listener);
	ball.yPosProperty().addListener(listener);

	return racket;
    }

    protected void initRackets(boolean leftAuto, boolean rightAuto) {
	double leftX = field.getLeftBoundX() - AbstractRacket.getWidth();
	double rightX = field.getRightBoundX();
	double y = field.getTopWallY();

	System.out.println(String.valueOf(leftAuto) + String.valueOf(rightAuto));
	if (leftAuto)
	    leftRacket = initAutoRacket(leftX, y);
	else
	    leftRacket = initUserRacket(leftX, y, KeyCode.A, KeyCode.Z);

	if (rightAuto)
	    rightRacket = initAutoRacket(rightX, y);
	else
	    rightRacket = initUserRacket(rightX, y, KeyCode.K, KeyCode.J);
    }

    protected void initTimelines() {
	ballTimeline.getKeyFrames().add(new KeyFrame(Duration.ZERO));
	ballTimeline.setOnFinished((ActionEvent e) -> updateBall(e));

	racketTimeline.getKeyFrames().add(new KeyFrame(Duration.ZERO));
	racketTimeline.setOnFinished((ActionEvent e) -> updateRackets(e));

	ballTimeline.play();
	racketTimeline.play();
    }

    protected abstract void updateRackets(ActionEvent e); 
    protected abstract void updateBall(ActionEvent e);

    protected Ball ball = new Ball();
    protected Field field = new Field();
    protected Score score;
    
    protected AbstractRacket leftRacket;
    protected AbstractRacket rightRacket;

    protected Timeline ballTimeline = new Timeline();
    protected Timeline racketTimeline = new Timeline();

    protected Scene scene;
}
