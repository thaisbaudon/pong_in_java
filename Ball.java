import javafx.scene.shape.Circle;
import javafx.scene.Node;
import javafx.beans.property.*;
import javafx.scene.paint.Color;
import java.lang.Math;

public class Ball implements Displayable {
    Ball() {
	this(100.0, 100.0);
    }

    Ball(double _xPos, double _yPos) {
	xPos = new SimpleDoubleProperty(_xPos);
	yPos = new SimpleDoubleProperty(_yPos);
	speed = randomSpeed();
	circle = new Circle(_xPos, _yPos, radius, Color.BLUE);

	// Synchronize the internal coords with the displayed coords
	circle.centerXProperty().bind(xPos);
	circle.centerYProperty().bind(yPos);
    }

    // Displayable implementation
    @Override
    public Node getNode() {
	return circle;
    }

    public void pickRandomSpeed() {
	speed = randomSpeed();
    }

    static private Vector randomSpeed() {
	double angle = Math.random() * 2 * Math.PI;
	double xSp = Math.cos(angle);
	double ySp = Math.sin(angle);
	Vector sp = new Vector(xSp, ySp);
	return sp.mult(scalarSpeed);
    }

    DoubleProperty xPosProperty() {
	return xPos;
    }
    
    double getXPos() {
	return xPos.get();
    }

    DoubleProperty yPosProperty() {
	return yPos;
    }
    
    double getYPos() {
	return yPos.get();
    }

    double getRadius() {
	return radius;
    }

    Vector getSpeed() {
	return new Vector(speed);
    }

    Vector getPos() {
	return new Vector(xPos.get(), yPos.get());
    }

    void setSpeed(Vector _speed) {
	speed.x = _speed.x;
	speed.y = _speed.y;
    }

    double getXSpeed() {
	return speed.x;
    }

    double getYSpeed() {
	return speed.y;
    }

    void setPos(Vector newPos) {
	xPos.set(newPos.x);
	yPos.set(newPos.y);
    }

    void reset(Vector resetPos) {
	// TODO: compute some new speed ?
	setPos(resetPos);
    }

    private DoubleProperty xPos;
    private DoubleProperty yPos;
    private static double radius = 10.0;
    private Vector speed;
    private Circle circle;
    private static double scalarSpeed = 0.20;
}
