import javafx.scene.shape.Line;
import javafx.scene.*;

public class Field implements Displayable, Bounceable {
    // Displayable
    @Override
    public Node getNode() {
	Line topWall = new Line(leftX, topWallY, rightX, topWallY);
	Line bottomWall = new Line(leftX, bottomWallY, rightX, bottomWallY);
	Group walls = new Group(topWall, bottomWall);
	return walls;
    }

    // Bounceable
    @Override
    public Vector bounce(Ball ball) {
	    return new Vector(ball.getXSpeed(), - ball.getYSpeed());
    }

    @Override
    public boolean ballHit(Ball ball) {
	double ballX = ball.getXPos();
	double ballY = ball.getYPos();
	double rad = ball.getRadius();

	boolean hitBottomWall = ballY - rad <= topWallY;

	boolean hitTopWall = ballY + rad >= bottomWallY;

	boolean hit = ball.getXPos() - ball.getRadius() > leftX;
	hit = hit && (ball.getXPos() + ball.getRadius() < rightX);
	hit = hit && (hitBottomWall || hitTopWall);

	return hit;
    }

    boolean outOfBounds(Ball ball) {
	boolean oob = ball.getXPos() - ball.getRadius()
			< leftX - collisionPadding;

	oob = oob || (ball.getXPos() + ball.getRadius()
			> rightX + collisionPadding);

	oob = oob || (ball.getYPos() - ball.getRadius()
			< topWallY - collisionPadding);

	oob = oob || (ball.getYPos() + ball.getRadius()
			> bottomWallY + collisionPadding);

	return oob;
    }

    Vector getCenterPos() {
	return new Vector(centerPos);
    }

    double getCenterPosX() {
	return centerPos.x;
    }

    double getCenterPosY() {
	return centerPos.y;
    }

    public double getTopWallY() {
	return topWallY;
    }

    public double getBottomWallY() {
	return bottomWallY;
    }

    public double getLeftBoundX() {
	return leftX;
    }

    public double getRightBoundX() {
	return rightX;
    }

    private static double topWallY = 10.0;
    private static double bottomWallY = 300.0;
    private static double leftX = 10.0;
    private static double rightX = 600.0;

    private static Vector centerPos =
	    new Vector((leftX + rightX) / 2, (topWallY + bottomWallY) / 2);
   
    private static double collisionPadding = 1.0;
};
